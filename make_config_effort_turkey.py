#!/usr/bin/env python3
# Script to produce config.yaml file for snakemake. This will be printed in the working directory.
# use:
# ./make_config.py <dir_with_fastq_files>



import os
import sys

# get path from commandline
path = sys.argv[1]

# get files from path
files = os.listdir( path )

# make dict of sample names
samples = dict()
for f in files:
    f_elem = f.split("_")
    sam = "_".join( f_elem[0:3])
    # check if the sample is in the dict
    if sam in samples.keys():
        # add file to list of files from sample
        samples[sam].append(f)  
    else:
        samples[sam] = [ f ] 

# print config file
outfile = open("config.yaml", "w" )
outfile.write("sample:\n")

# add each sample
for sam, fil in samples.items():
    outfile.write("    {}:\n".format(sam))
    for i in [1,2]:
        # get read number
        r = i
        read = "R" + str(r)
        outfile.write("        {}:\n".format(read))
        # make sure the files are sorted
        fil.sort()
        for f in fil:
            # add the files of the right read
            if read in f:
                outfile.write("            {}\n".format(f))
    





