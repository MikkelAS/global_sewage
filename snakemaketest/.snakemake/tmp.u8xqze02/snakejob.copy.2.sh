#!/bin/sh
# properties = {"params": {}, "local": false, "cluster": {}, "resources": {}, "output": ["data/b_cp.txt"], "jobid": 2, "threads": 4, "input": ["data/b.txt"], "log": [], "rule": "copy", "wildcards": ["b"]}
cd /home/projects/cge/apps/global_sewage/snakemaketest && \
/services/tools/anaconda3/4.0.0/bin/python \
-m snakemake data/b_cp.txt --snakefile /home/projects/cge/apps/global_sewage/snakemaketest/Snakefile \
--force -j --keep-target-files --keep-remote \
--wait-for-files /home/projects/cge/apps/global_sewage/snakemaketest/.snakemake/tmp.u8xqze02 data/b.txt --latency-wait 5 \
--benchmark-repeats 1 --attempt 1 \
--force-use-threads --wrapper-prefix https://bitbucket.org/snakemake/snakemake-wrappers/raw/ \
   --nocolor \
--notemp --no-hooks --nolock --timestamp  --force-use-threads  --allowed-rules copy  && touch "/home/projects/cge/apps/global_sewage/snakemaketest/.snakemake/tmp.u8xqze02/2.jobfinished" || (touch "/home/projects/cge/apps/global_sewage/snakemaketest/.snakemake/tmp.u8xqze02/2.jobfailed"; exit 1)

