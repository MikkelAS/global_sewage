#!/bin/sh
# properties = {"output": ["data/c_cat.txt"], "log": [], "wildcards": ["c"], "cluster": {}, "threads": 1, "params": {}, "jobid": 4, "resources": {}, "input": ["data/c_cp.txt"], "rule": "cat", "local": false}
cd /home/projects/cge/apps/global_sewage/snakemaketest && \
/services/tools/anaconda3/4.0.0/bin/python \
-m snakemake data/c_cat.txt --snakefile /home/projects/cge/apps/global_sewage/snakemaketest/Snakefile \
--force -j --keep-target-files --keep-remote \
--wait-for-files /home/projects/cge/apps/global_sewage/snakemaketest/.snakemake/tmp.fddzlj87 data/c_cp.txt --latency-wait 5 \
--benchmark-repeats 1 --attempt 1 \
--force-use-threads --wrapper-prefix https://bitbucket.org/snakemake/snakemake-wrappers/raw/ \
   --nocolor \
--notemp --no-hooks --nolock --timestamp  --force-use-threads  --allowed-rules cat  && touch "/home/projects/cge/apps/global_sewage/snakemaketest/.snakemake/tmp.fddzlj87/4.jobfinished" || (touch "/home/projects/cge/apps/global_sewage/snakemaketest/.snakemake/tmp.fddzlj87/4.jobfailed"; exit 1)

