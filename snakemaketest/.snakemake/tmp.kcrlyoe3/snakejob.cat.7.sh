#!/bin/sh
# properties = {"output": ["data/c_cat.txt"], "rule": "cat", "jobid": 7, "resources": {}, "cluster": {}, "local": false, "params": {}, "wildcards": ["c"], "input": ["data/c_cp.txt"], "threads": 1, "log": []}
cd /home/projects/cge/apps/global_sewage/snakemaketest && \
/services/tools/anaconda3/4.0.0/bin/python \
-m snakemake data/c_cat.txt --snakefile /home/projects/cge/apps/global_sewage/snakemaketest/Snakefile \
--force -j --keep-target-files --keep-remote \
--wait-for-files /home/projects/cge/apps/global_sewage/snakemaketest/.snakemake/tmp.kcrlyoe3 data/c_cp.txt --latency-wait 5 \
--benchmark-repeats 1 --attempt 1 \
--force-use-threads --wrapper-prefix https://bitbucket.org/snakemake/snakemake-wrappers/raw/ \
   --nocolor \
--notemp --no-hooks --nolock --timestamp  --force-use-threads  --allowed-rules cat  && touch "/home/projects/cge/apps/global_sewage/snakemaketest/.snakemake/tmp.kcrlyoe3/7.jobfinished" || (touch "/home/projects/cge/apps/global_sewage/snakemaketest/.snakemake/tmp.kcrlyoe3/7.jobfailed"; exit 1)

