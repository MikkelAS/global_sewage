#!/bin/sh
### Note: No commands may be executed until after the #PBS lines
### Account information
#PBS -W group_list=cge -A cge
### Job name (comment out the next line to get the name of the script used as the job name)
#PBS -N snakemake
### Output files (comment out the next 2 lines to get the job name used instead)
#PBS -e snakemake.err
#PBS -o snakemake.log
### Number of nodes
#PBS -l nodes=1:ppn=01,walltime=1000:00:00
### Add current shell environment to job (comment out if not needed)
#PBS -V
#PBS -d /home/projects/cge/apps/global_sewage/
### add $PBS_ARRAYIDs 
##PBS -t 1-7

areas=( africa/ asia/ europe/ middle_east/ north_america/ south_america/ oceania/)
ID=${areas[${PBS_ARRAYID}]}
ID=""
force="--forcerun repres_genes"
rule="get_ffn"
config="--configfile /home/projects/cge/analysis/global_sewage/reducing_dark_matter/"$ID"config.yaml"


#snakemake --unlock --resources  mem_gb=990 --cluster 'qsub -W group_list=cge -A cge -l nodes=1:ppn={threads},mem={resources.mem_gb}gb,walltime={resources.runtime} -V -d /home/projects/cge/analysis/global_sewage/reducing_dark_matter/'$ID --directory /home/projects/cge/analysis/global_sewage/reducing_dark_matter/$ID --jobs 1000 --jobname "sj.{name}.{jobid}" $config index

snakemake --resources  mem_gb=990 --cluster 'qsub -W group_list=cge -A cge -l nodes=1:ppn={threads},mem={resources.mem_gb}gb,walltime={resources.runtime} -V -d /home/projects/cge/analysis/global_sewage/reducing_dark_matter/'$ID --directory /home/projects/cge/analysis/global_sewage/reducing_dark_matter/$ID --jobs 1000 --jobname "sj.{name}.{jobid}" $force $config $rule


