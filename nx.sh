#!/bin/sh
### Account information
#PBS -W group_list=cge -A cge
### Number of nodes
#PBS -l nodes=1:ppn=4:thinnode
### Requesting time
#PBS -l walltime=03:00:00
### Here follows the user commands:
# Load all required modules for the job
module load tools
module load moab torque
module load anaconda3/4.0.0
module load pigz/2.3.4
perl/5.20.1

result="/home/projects/cge/analysis/global_sewage/reducing_dark_matter/data_summary/all_scaffolds.stats"
for conti in africa asia europe middle_east north_america oceania
do
  echo $conti >> $result
  
  for d in /home/projects/cge/analysis/global_sewage/reducing_dark_matter/$conti/assembly/*; do
  echo $d
  perl /home/projects/cge/analysis/global_sewage/reducing_dark_matter/south_america/nx.pl  "$d" 10,20,30,40,50 >> $result;
  done
done
