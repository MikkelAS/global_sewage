#!/bin/sh
### Note: No commands may be executed until after the #PBS lines
### Account information
#PBS -W group_list=cge -A cge
### Job name (comment out the next line to get the name of the script used as the job name)
#PBS -N snakemake
### Output files (comment out the next 2 lines to get the job name used instead)
#PBS -e snakemake.err
#PBS -o snakemake.log
### Number of nodes
#PBS -l nodes=1:ppn=01,walltime=1000:00:00
### Add current shell environment to job (comment out if not needed)
#PBS -V
#PBS -d /home/projects/cge/apps/global_sewage/



rule="get_ffn"
#rule="test"
config="--configfile /home/projects/cge/analysis/global_sewage/reducing_dark_matter/mikkel_sandbox/submit_test/config.yaml"
showcase="--forceall -n"
dag="--dag $rule"

if [[ $1 = "cluster" ]]
then
  snakemake --cluster 'qsub -W group_list=cge -A cge -l nodes=1:ppn={threads},mem={resources.mem_gb}gb,walltime={resources.runtime} -V -d /home/projects/cge/analysis/global_sewage/reducing_dark_matter/mikkel_sandbox/submit_test' --directory /home/projects/cge/analysis/global_sewage/reducing_ddark_matter/mikkel_sandbox/submit_test --jobs 1000 --jobname "sj.{name}.{jobid}" -s Snakefile_1.1 $rule
else
  snakemake --directory /home/projects/cge/analysis/global_sewage/reducing_dark_matter/mikkel_sandbox -C min_gene=10 $1 --nolock $config  -s Snakefile_1.1 $showcase $rule
fi

