
dir="/home/projects/cge/analysis/global_sewage/reducing_dark_matter/"
#dir="/home/projects/cge/analysis/EFFORT/EFFORT_all_new/data/EFFORT_2016/trim/1011-17-001_002_trim/"
areas=( africa asia europe middle_east north_america south_america oceania )
for a in "${areas[@]}"
do
    echo $a
    cd $dir$a
    $gs_apps/make_config.py TrimmedData
done

