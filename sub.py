#!/usr/bin/env python3
# coding=utf-8
import argparse
import os
import time
import re

def get_args(args=None):
    """
    Parse command line arguments to a namespace with values. 
    By default used with args=None to run it from command line but can be run using another script by setting args.
    :param args: string arguments or list of arguments.
    :return: namespace
    """
    # check function arguments
    if isinstance(args, str): args = args.split(' ')
    
    parser = argparse.ArgumentParser(description="A general submitter script. This should ideally be used for most submissions.")
    parser.add_argument("script", help="A string indicating the code to run.")
    parser.add_argument("-n", "--name",
                        help="Name of the submission job. Default is a unique number that doesn't create conflict.")
    parser.add_argument("-num", "--number", action='store_true', help="Add a number after the name in filenames to avoid overwriting files.")
    parser.add_argument("-minutes", "--minutes", dest="minutes", type=int, default=0,
                        help="Wall-time minutes. Default is 0 unless no kind of wall-time is provided, then it is 30.")
    parser.add_argument("-hours", "--hours", type=int, default=0, help="Wall-time hours. Default is 0.")
    parser.add_argument("-mem", "--memory", help="Memory in gb. Default is 20.", default=20)
    parser.add_argument("-p", "--project", type=str, dest="project", default="cge" ,
                        help="Project to be billed for the run.")
    parser.add_argument("-dir", "--workdir", default=os.getcwd(), 
                        help="The submission files are saved here. Relative filenames in command is relative to this. "
                             "Default is current directory.")
    parser.add_argument("-py2", "--python2", action='store_true', help="A flag set if we want to run the code with python2.")
    parser.add_argument("-np", "--n_processes", type=int, dest="nproc", metavar="integer", default=1, 
                        help="Number of processors to use for work")

    parser.add_argument("-w", "--wait", type=int, dest="wait_for",
                        help="Used to wait of a given job to finish successfully. Insert the job number.")
    parser.add_argument("-W", "--wait-for-array", type=str, dest="wait_for_array",
                        help="Used to wait for an array job to finish successfully. Insert the job number including []")
    parser.add_argument("-a", "--array", type=str,
                        help="Used to create job array, the job number will be $PBS_ARRAYID. Insert numbers to run eg. 608-631.")
    parser.add_argument("-max_jobs", "--max_jobs", type=int, default=10, 
                        help="Choose maximum number of jobs to run at a time from this call. "
                             "This setting is only relevant for array jobs. "
                             "If more array jobs are started than this number, some of them will wait to run.")

    args = parser.parse_args(args)
    if not args.name: args.number = True
    if args.number:
        root = args.workdir + "/sub"
        if args.name: root += "_" + args.name
        # find out what the outfiles are gonna be named if avoid overwrite using incrementing numbers
        outfnames = number_files([root + ext for ext in ['.qsub', '.out', '.err']])
        # the name is then changed to include an appended number
        args.name = os.path.splitext(os.path.basename(outfnames[0]))[0][len('sub_'):]
        print("# job name changed to", args.name)

    if not args.minutes and not args.hours: args.minutes = 30

    if args.array:
       # # validate format of inputs
       # array_range = args.array.split("-")
       # if len(array_range) != 2: parser.error("-a needs two '-' separated values eg. 608-631")
       # try:
       #     [int(value) for value in array_range]
       # except ValueError:
       #     parser.error("-a needs two '-' separated values eg 608-631")

        if "$PBS_ARRAYID" not in args.script:
            parser.error("When using -a the script needs to contain $PBS_ARRAYID")

    print(args)
    return args


def get_walltime(hours=0, minutes=0, seconds=0):
    walltime = "%d:%2d:%2d" % (hours, minutes, seconds)
    walltime = walltime.replace(' ', '0')
    return walltime


def get_array_PBS(array, n_jobs):
    if array: return "#PBS -t " + array + "%" + str(n_jobs)
    return ""


def number_files(filenames, max_number=99):
    """
    Get filenames for each given that has a number appended. The number increases to avoid file conflict, 
    so we don't overwrite existing files.
    :param filenames: single filename or list of filenames to give a number (the same number for all)
    :param max_number: maximum number to give. If this is reached it will be used even if it overwrites.
    :return: filename(s) to use as single string or list of strings
    """
    is_single = isinstance(filenames, str)
    if is_single: filenames = [filenames]
    
    for i in range(max_number + 1):
        names = [add_ending(name, str(i).rjust(len(str(max_number)), '0')) for name in filenames]
        exists = [os.path.isfile(name) for name in names]
        if any(exists): continue
        # non exists if we get to this point
        if is_single: return names[0]
        return names

def add_ending(fname, ending):
    """Add an ending after a '_' to a filename before the file extension.
    If there is a period in the ending, it is assumed this means the extension is chosen to be the part after the period.
    If the ending is only an extension, a change of extension is performed.
    :param fname string
    :param ending string or number
    :return string"""
    fname = fname.rstrip('_')
    ending = str(ending).strip('_')
    assert ending
    root, extension = path_split_extension(fname)
    if root and not ending.startswith('.'): root += '_'
    if '.' in ending:
        return root + ending
    if extension:
        return root + ending + extension
    return root + ending + '.fa'


def path_split_extension(fname):
    """
    Like os.path.splitext but splits according to a double extension if one is present.
    :param fname: string name
    :return: 
    """
    root, extension = os.path.splitext(fname)
    root, extra_extension = os.path.splitext(root)
    if re.match('\.[a-zA-Z]+', extra_extension):
        # it is in fact another extension
        return root, extra_extension + extension
    # it is not a real extension
    return root + extra_extension, extension

def write_qsub(name, script, nproc=1, memory=20, walltime='1:00:00', workdir=None, python=3, wait_for=None, 
    wait_for_array=None, extra_PBS="", project="cge"):
    """
    Write a qsub file that can be run on computerome.
    :param name: name of the job
    :param script: string that is the code to run.
    :param nproc: number of processors to use.
    :param memory: memory in gb to use.
    :param walltime: walltime string indicating walltime to allocate to job.
    :param workdir: the path to tun the code in. Default is using current working directory.
    :param python: version of python to use.
    :param extra_PBS: extra PBS lines to add at the end of the usual ones.
    :return: the filename of the qsub file.
    """
    if workdir: workdir = os.path.abspath(workdir)
    else: workdir = os.getcwd()

    if type(memory) is not str: memory = str(memory) + 'gb'


    qsub_string = \
        '#!/bin/sh\n' \
        '### Note: No commands may be executed until after the #PBS lines\n' \
        '### Account information\n' \
        '#PBS -W group_list={project} -A {project}\n'.format(project=project)

    if wait_for:
        qsub_string += \
            '#PBS -W depend=afterok:{wait_for}\n'.format(wait_for=wait_for)

    if wait_for_array:
        qsub_string += \
            '#PBS -W depend=afterokayarray:{wait_for_array}\n'.format(wait_for_array=wait_for_array)
    qsub_string += \
        '### Job name (comment out the next line to get the name of the script used as the job name)\n' \
        '#PBS -N sym_{name}\n' \
        '### Output files (comment out the next 2 lines to get the job name used instead)\n' \
        '#PBS -e {workdir}/sub_{name}.err\n' \
        '#PBS -o {workdir}/sub_{name}.out\n' \
        '### Email: no (n)\n' \
        '#PBS -M n\n' \
        '### Make the job rerunable (y)\n' \
        '#PBS -r y\n' \
        '### Number of nodes\n' \
        '#PBS -l nodes=1:ppn={nproc}:thinnode\n' \
        '#PBS -l walltime={walltime}\n' \
        '#PBS -l mem={memory}\n' \
        '{extra}\n' \
        'echo This is the STDOUT stream from a PBS Torque submission script.\n' \
        '# Go to the directory from where the job was submitted (initial directory is $HOME)\n' \
        'echo Working directory is $PBS_O_WORKDIR\n' \
        'cd $PBS_O_WORKDIR\n' \
        '\n' \
        '# Get number of processors\n' \
        'export NPROC={nproc}\n' \
        'echo This job has allocated $NPROC nodes\n' \
        '\n' \
        '# Load user Bash settings:\n' \
        'source /home/projects/cge/apps/global_sewage/.bash_gs\n' \
        ''.format(extra=extra_PBS, name=name, nproc=nproc, memory=memory, walltime=walltime, workdir=workdir)

    if '2' in str(python):
        qsub_string += \
            'module unload anaconda3/4.0.0\n' \
            'module load anaconda2/4.0.0\n'

    qsub_string += \
        '\n' \
        'echo  Now the user defined script is run. After the ---- line, the STDOUT stream from the script is pasted.\n' \
        'echo -----------------------------------------------------------------------------------------------------\n' \
        '\n' + script + '\n'

    qsub_string += \
        'sleep 5\n' \
        'exit 0\n'

    qsub_filename = '{workdir}/sub_{name}.qsub'.format(workdir=workdir, name=name)
    with open(qsub_filename, 'w') as outfile: outfile.write(qsub_string)

    return qsub_filename


def submit(fname):
    start_dir = os.getcwd()
    os.chdir(os.path.dirname(fname))
    os.system('qsub %s' % os.path.basename(fname))
    time.sleep(1)
    os.chdir(start_dir)


def main(args):
    walltime = get_walltime(args.hours, args.minutes)
    if args.python2: python = 2
    else: python = 3

    array_string = get_array_PBS(args.array, args.max_jobs)
    fname = write_qsub(args.name, args.script, args.nproc, args.memory, walltime, args.workdir, python, 
        args.wait_for, args.wait_for_array, array_string, project = args.project)
    submit(fname)


if __name__ == "__main__":
    main(get_args())
    print("# submitted")


