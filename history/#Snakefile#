
import os
import math

# $ snakemake --dag all | dot -Tsvg > dag.svg
IDS = glob_wildcards("TrimmedData/{sample,[^_]+}_{number,.*}R1.trim.fq.gz")

# get memory needed for the assembly rule.
def get_assembly_mem(file_name):
    file_size = os.path.getsize(file_name)
    mem_needed = (file_size / 1000000000) * 120
    return max( math.ceil( mem_needed ), 100 )

print(IDS)

rule cat:
    input:
        R1=lambda wildcards: expand( "TrimmedData/{sample}_{number}R1.trim.fq.gz", number=[ IDS.number[j] for j in  [i for i, x in enumerate(IDS.sample) if x == wildcards.sample] ], sample = wildcards.sample),
        R2=lambda wildcards: expand( "TrimmedData/{sample}_{number}R2.trim.fq.gz", number=[ IDS.number[j] for j in  [i for i, x in enumerate(IDS.sample) if x == wildcards.sample] ], sample = wildcards.sample)
    output:
        R1="cat_data/{sample}_R1.trim.fq.gz",
        R2="cat_data/{sample}_R2.trim.fq.gz" 
    threads: 1
    resources:
        mem_gb = 50,
        runtime = 3600
    run:
        R1_files = ""
        R2_files = ""
        for i in range(len(input.R1)):
            R1_files += " {R1} ".format(R1 = input.R1[i])
            R2_files += " {R2} ".format(R2 = input.R2[i])
        shell(  "mkdir -p cat_data/; cat" + R1_files + " > {output.R1}; cat" +  R2_files + " > {output.R2}")

rule cat2:
    input:
        R1="cat_data/{sample}_R1.trim.fq.gz",
        R2="cat_data/{sample}_R2.trim.fq.gz"
    output:"test"
    shell: "cat {input} > {output}"

rule assembly:
    input:
        R1="cat_data/{sample}_R1.trim.fq.gz",
        R2="cat_data/{sample}_R2.trim.fq.gz"
    output:
        "assembly/{sample}/scaffolds.fasta"
    threads: 8
    resources:
        mem_gb = lambda wildcards:  get_assembly_mem( "cat_data/" + wildcards.sample + "_R1.trim.fq.gz" ),
        runtime = 864000
    params:
        outfolder = "assembly/{sample}"
    run:
        # find path for output .paths file - will indicate whether spades has been run before and can be rerun.  
        import os.path
        path_file =  output[0].split(".") 
        path_file.pop()
        path_file = ".".join(path_file) + ".paths"
        print(path_file)

        
        if os.path.isfile( path_file ):
            shell( "spades.py --restart-from last -o $(dirname {output})" )
        else:
            shell( "mkdir -p {params.outfolder}; time spades.py --meta -m {resources.mem_gb} --phred-offset 33 --pe1-1 {input.R1} --pe1-2 {input.R2} -o $(dirname {output}) -t {threads}" )


rule rename:
    input:
        "assembly/{sample}/scaffolds.fasta"
    output:
        "assembly/scaffolds/{sample}.fasta"
    threads: 1
    resources:
        mem_gb = 20,
        runtime = 7200
    params:
        sample = "{sample}" 
    shell:
        "sed -i -re '/^>/s/_/ /2' {input}; sed  s/NODE/{params.sample}/g {input} > {output}"

rule gene_predict:
    input:
        "assembly/scaffolds/{sample}.fasta"
    output:
        genes = "genes/coordinates/{sample}.genes",
        protein = "genes/protein/{sample}.faa",
        nuceotides = "genes/nucleotide/{sample}.ffn"
    threads: 1
    resources:
        mem_gb = 20,
        runtime = 43200
    params:
        sample = "{sample}"
    shell:
        "prodigal -i {input} -o {output.genes} -a {output.protein} -d {output.nuceotides} -p meta"
        
rule combine_genes:
    input:
        prot = expand("genes/protein/{sample}.faa", sample=list(set(IDS.sample))),
        nuc = expand("genes/nucleotide/{sample}.ffn", sample=list(set(IDS.sample)))
    output:
        prot = "genes/all_genes.faa",
        nuc = "genes/all_genes.ffn"
    threads: 1
    resources:
        mem_gb = 60,
        runtime = 10000
    shell:
        "cat {input.prot} > {output.prot}; cat {input.nuc} > {output.nuc}"


rule all_genes:
    input:
        "genes/all_genes.faa"

rule repres_genes:
    input:
        "genes/all_genes.faa"
    output:
        "genes/all_genes_rep_seq.fasta"
    threads: 10
    resources:
        mem_gb = 50,
        runtime = 86400
    shell:
        "pcregrep -M '^>[^>]*\n[^>]{{101,}}' {input} > genes/temp.faa ; mmseqs easy-linclust --threads {threads} genes/temp.faa $( echo {output} | cut -d'_' -f1,2 ) tmp; rm genes/temp.faa" 

rule get_ffn:
    input:
        prot = "genes/all_genes_rep_seq.fasta",
        nuc = "genes/all_genes.ffn"
    output:
        "genes/all_genes_rep_seq.ffn"
    threads: 10
    resources:
        mem_gb = 50,
        runtime = 86400
    run:
        from Bio import SeqIO
        fasta_file = input.nuc  # Input fasta file
        id_file = input.prot # Input id file
        result_file = output[0] # Output fasta file
        
        wanted = set()
        f = open(id_file, "r")
        for line in f:
            line = line.strip()
            if line[0] == ">":
                wanted.add(line.split()[0].strip(">"))
        fasta_sequences = SeqIO.parse(open(fasta_file,"r"),'fasta')
        end = False
        f = open(result_file, "w")
        
        for seq in fasta_sequences:
            
            if seq.id in wanted:
                SeqIO.write([seq], f, "fasta")

rule index:
    input:
       "genes/all_genes_rep_seq.ffn" 
    output:
       fasta="mapping/index/all_genes_rep_seq.ffn",
       index_part="mapping/index/all_genes_rep_seq.ffn.bwt"
    threads: 1
    resources:
        mem_gb = 50,
        runtime = 86400
    shell:
        "mkdir -p $(dirname {output.fasta}); ln -s $(readlink -f {input}) {output.fasta}; bwa index {output.fasta}"

rule map:
    input:
        index="mapping/index/all_genes_rep_seq.ffn",
        R1="cat_data/{sample}_R1.trim.fq.gz",
        R2="cat_data/{sample}_R2.trim.fq.gz" 
    output:
        "mapping/{sample}.bam"
    threads: 12
    resources:
        mem_gb = 50,
        runtime = 86400
    shell:
        "bwa mem -t 12 {input.index} {input.R1} {input.R2} | samtools view -Sb -f4 - >  {output}"

# decide on which assembly method to use (default is vamb)
if "binning" not in config:
    config["binning"] = "vamb"

if config["binning"].lower()  == "metabat":
    rule binning:
        input: 
            bam = expand("mapping/{sample}.bam", sample=list(set(IDS.sample))),
            scaffolds = "mapping/index/all_genes_rep_seq.ffn"
        output:
            depth = "{input.scaffolds}.depth.txt",
            bin1 = "{input.scaffolds}.metabat-bins/bin.1.fa"
        threads: 32
        resources:
            mem_gb = 990,
            runtime = 1209600
        shell: 'runMetaBat.sh {input.scaffolds} {input.bam}'

else: 
    rule binning:
        input:
            bam = expand("mapping/{sample}.bam", sample=list(set(IDS.sample))),
            scaffolds = "mapping/index/all_genes_rep_seq.ffn"
        output:
            cluster_info = "vamb/clusters.tsv"
        threads: 32
        resources:
            mem_gb = 990,
            runtime = 1209600
        params:
            outfolder = "vamb",
        shell: 'python /services/tools/vamb/20181101/vamb/run.py {params.outfolder} {input.scaffolds} {input.bam}'



