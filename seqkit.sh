#!/bin/sh
### Account information
#PBS -W group_list=cge -A cge
### Number of nodes
#PBS -l nodes=1:ppn=4:thinnode
### Requesting time
#PBS -l walltime=01:00:00
### Here follows the user commands:
# Load all required modules for the job
module load tools
module load moab torque
module load anaconda3/4.0.0
module load pigz/2.3.4
module load seqkit/0.7.1

find /home/projects/cge/analysis/global_sewage/reducing_dark_matter/assembly -name "contigs.fasta" -print0 | while read -d $'\0' file; do
seqkit stats  "$file" -a >> /home/projects/cge/analysis/global_sewage/reducing_dark_matter/seqkit.stats;
done
sed -n '1p;2~2p' /home/projects/cge/analysis/global_sewage/reducing_dark_matter/seqkit.stats >  /home/projects/cge/analysis/global_sewage/reducing_dark_matter/seqkit.table
rm /home/projects/cge/analysis/global_sewage/reducing_dark_matter/seqkit.stats
