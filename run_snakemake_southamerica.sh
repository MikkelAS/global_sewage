#!/bin/sh
### Note: No commands may be executed until after the #PBS lines
### Account information
#PBS -W group_list=cge -A cge
### Job name (comment out the next line to get the name of the script used as the job name)
#PBS -N snakemake
### Output files (comment out the next 2 lines to get the job name used instead)
#PBS -e snakemake_south_america.err
#PBS -o snakemake_south_america.log
### Number of nodes
#PBS -l nodes=1:ppn=01,walltime=1000:00:00
### Add current shell environment to job (comment out if not needed)
#PBS -V
#PBS -d /home/projects/cge/apps/global_sewage/


snakemake --forcerun  binning --resources mem_gb=990 --rerun-incomplete --snakefile /home/projects/cge/apps/global_sewage/Snakefile --cluster 'qsub -W group_list=cge -A cge -l nodes=1:ppn={threads},mem={resources.mem_gb}gb,walltime={resources.runtime} -V -d /home/projects/cge/analysis/global_sewage/reducing_dark_matter/south_america' --directory /home/projects/cge/analysis/global_sewage/reducing_dark_matter/south_america --jobs 1000 --jobname "sj.{name}.{jobid}" index
