cd $2

# make dirs
mkdir -p TrimmedData
mkdir -p cat_data
mkdir -p assembly

dir="/home/projects/cge/analysis/global_sewage/reducing_dark_matter/"
# go trough file and make links
while read s
do
  sample=$( echo $s | cut -f1 -d"_" )
  # Original data
  ln -s $dir"TrimmedData/"$s"_R1.trim.fq.gz" "TrimmedData/"$s"_R1.trim.fq.gz" 
  ln -s $dir"TrimmedData/"$s"_R2.trim.fq.gz" "TrimmedData/"$s"_R2.trim.fq.gz"

done < $1

sleep 1m
while read s
do
  # cat data
  if [[ $s == *"_"* ]]  
  then 
    continue 
  fi
  ln -s $dir"cat_data/"$s"_R1.trim.fq.gz" "cat_data/"$s"_R1.trim.fq.gz"
  ln -s $dir"cat_data/"$s"_R2.trim.fq.gz" "cat_data/"$s"_R2.trim.fq.gz"

done < $1

sleep 1m
while read s
do
  # cat data
  if [[ $s == *"_"* ]]
  then
    continue
  fi
  # assembly and genes
  
  ln -s $dir"assembly/"$s"/scaffolds.fasta" assembly/$s
done < $1


sleep 1m
mkdir -p genes/nucleotide/
mkdir -p genes/protein/
mkdir -p genes/coordinates/
while read s
do
  # cat data
  if [[ $s == *"_"* ]]
  then
    continue
  fi  
  ln -s $dir"genes/nucleotide/"$s".ffn" "genes/nucleotide/"
  ln -s $dir"genes/protein/"$s".faa" "genes/protein/"
  ln -s $dir"genes/coordinates/"$s".genes" "genes/coordinates/"

done < $1

cd ..
